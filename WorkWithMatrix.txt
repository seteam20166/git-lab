Unit WorkWithMatrix;

Interface

 const SizeOfMatrix = 5;
       MinNumberOfDiapason = -7;
       MaxNumberOfDiapason = 5;

 type MatrixType = array [1..SizeOfMatrix, 1..SizeOfMatrix] of Real; 

 function GetTheMinimumElementOfTheMainDiagonal(matrix: MatrixType; var position: Integer): Real;
 function IsTheElementLocatedUnderTheSecondaryDiagonal(i, j: Integer): Boolean;
 function GetTheAmountOfTheElementsLocatedUnderSecondaryDiagonal(matrix: MatrixType): Integer;
 function GetTheSumOfTheElementsLocatedUnderSecondaryDiagonal(matrix: MatrixType): Real;
 function GetTheArithmeticAverageOfTheElementsLocatedUnderSecondaryDiagonal(sum: Real; count: Integer): Real;
 procedure ReplaceTheMinimumElementOfTheMainDiagonalByArithmeticAverage(var matrix: MatrixType; position: Integer; ArithmeticAverage: Real);
 
Implementation

 function GetTheMinimumElementOfTheMainDiagonal(matrix: MatrixType; var position: Integer): Real;
 var i, j: Integer;
      min: Real;
 begin
     min := matrix[1, 1];
     position := 1;
     for i := 2 to SizeOfMatrix do
     begin
         j := i;
         if (matrix[i, j] < min) then
         begin
             min := matrix[i, j];
             position := i
         end
     end;
     GetTheMinimumElementOfTheMainDiagonal := min
 end;

 function IsTheElementLocatedUnderTheSecondaryDiagonal(i, j: Integer): Boolean;
 begin
     if ((SizeOfMatrix - j + 1) < i) then
     begin
         IsTheElementLocatedUnderTheSecondaryDiagonal := True
     end
     else
     begin
         IsTheElementLocatedUnderTheSecondaryDiagonal := False
     end
 end;

 function GetTheAmountOfTheElementsLocatedUnderSecondaryDiagonal(matrix: MatrixType): Integer;
 var i, j, count: Integer;
 begin
     count := 0;
     for i := 1 to SizeOfMatrix do
     begin
         for j := 1 to SizeOfMatrix do
         begin
             if (IsTheElementLocatedUnderTheSecondaryDiagonal(i, j) = true) then
             begin
                 Inc(count)
             end
         end
     end;
     GetTheAmountOfTheElementsLocatedUnderSecondaryDiagonal := count
 end;
 
 function GetTheSumOfTheElementsLocatedUnderSecondaryDiagonal(matrix: MatrixType): Real;
 var i, j: Integer;
      sum: Real;
 begin
     sum := 0;
     for i := 1 to SizeOfMatrix do
     begin
         for j := 1 to SizeOfMatrix do
         begin
             if (IsTheElementLocatedUnderTheSecondaryDiagonal(i, j) = true) then
             begin
                 sum := sum + matrix[i, j]
             end
         end
     end;
     GetTheSumOfTheElementsLocatedUnderSecondaryDiagonal := sum
 end;

 function GetTheArithmeticAverageOfTheElementsLocatedUnderSecondaryDiagonal(sum: Real; count: Integer): Real;
 begin
     GetTheArithmeticAverageOfTheElementsLocatedUnderSecondaryDiagonal := sum / count
 end;

 procedure ReplaceTheMinimumElementOfTheMainDiagonalByArithmeticAverage(var matrix: MatrixType; position: Integer; ArithmeticAverage: Real);
 begin
     matrix[position, position] := ArithmeticAverage
 end;

end.
